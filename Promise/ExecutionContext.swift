//
// Created by Stephen Vickers on 10/29/18.
// Copyright (c) 2018 Stephen Vickers. All rights reserved.
//

import Foundation
#if os(Linux)
import Dispatch
#endif

public protocol ExecutionContext{
    func execute( _ work: @escaping () -> Void)
}

extension DispatchQueue: ExecutionContext{
    public func execute(_ work: @escaping () -> Void){
        self.async(execute: work)
    }
}