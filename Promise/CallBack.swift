//
// Created by Stephen Vickers on 10/29/18.
// Copyright (c) 2018 Stephen Vickers. All rights reserved.
//

import Foundation

#if os(Linux)
import Dispatch
#endif

struct CallBack<Value> {
    let onFulfilled: (Value) -> ()
    let onRejected: (Error) -> ()
    let queue: ExecutionContext

    func callFulfill(_ value : Value){
        self.queue.execute({
            self.onFulfilled(value)
        })
    }

    func callReject(_ error: Error){
        self.queue.execute({
            self.onRejected(error)
        })
    }


}
