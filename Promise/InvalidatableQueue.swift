//
// Created by Stephen Vickers on 10/29/18.
// Copyright (c) 2018 Stephen Vickers. All rights reserved.
//

import Foundation

#if os(Linux)
import Dispatch
#endif

public final class InvalidatableQueue: ExecutionContext{

    private var valid = true

    private let queue: DispatchQueue

    public init(queue: DispatchQueue = .main){
        self.queue = queue
    }

    public func invalidate(){
        self.valid = false
    }

    public func execute(_ work: @escaping () -> Void) {
        guard self.valid else {return}
        self.queue.async(execute: work)
    }
}