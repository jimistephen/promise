//
// Created by Stephen Vickers on 10/29/18.
// Copyright (c) 2018 Stephen Vickers. All rights reserved.
//

import Foundation

#if os(Linux)
import Dispatch
#endif

enum State<Value> {

    /*:
        The promise has not completed yet.
        will transition to either the `fulfilled` or the `rejected` state.
    */
    case pending

    /*:
        The promise now has a value.
        Will not transition to any other state
    */
    case fulfilled(value: Value)

    /*:
        The promise failed with the included error.
        Will not transition to any other state
    */
    case rejected(error: Error)

    var isPending: Bool{
        if case .pending = self{
            return true
        }else{
            return false
        }
    }

    var isFulfilled: Bool{
        if case .fulfilled = self{
            return true
        }else{
            return false
        }
    }

    var isRejected: Bool{
        if case .rejected = self{
            return true
        }else{
            return false
        }
    }

    var error: Error? {
        if case let .rejected(error) = self{
            return error
        }else{
            return nil
        }
    }

    var value: Value?{
        if case let .fulfilled(value) = self{
            return value
        }
        else{
            return nil
        }
    }
}

extension State: CustomStringConvertible{
    public var description: String {
        switch self{
            case .pending:
                return "Pending"
            case .fulfilled(let value):
                return "Fulfilled (\(value))"
            case .rejected(let error):
                return "Rejected (\(error))"
        }
    }
}
