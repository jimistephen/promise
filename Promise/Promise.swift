//
// Created by Stephen Vickers on 10/29/18.
// Copyright (c) 2018 Stephen Vickers. All rights reserved.
//

import Foundation

#if os(Linux)
    import Dispatch
#endif

private struct StaticPromise{
    fileprivate static let lockQueueName = "promise_lock_queue"
}

public final class Promise<Value>{

    public var isPending: Bool {
        return !isFulfilled && !isRejected
    }

    public var isFulfilled: Bool {
        return value != nil
    }

    public var isRejected: Bool {
        return error != nil
    }

    public var value: Value? {
        return lockQueue.sync(execute: {
            return self.state.value
        })
    }

    public var error: Error? {
        return lockQueue.sync(execute: {
            return self.state.error
        })
    }

    private var state: State<Value>
    private let lockQueue = DispatchQueue(label: StaticPromise.lockQueueName, qos: .userInitiated)
    private var callbacks: [CallBack<Value>] = []

    public init(){
        self.state = .pending
    }

    public init(value: Value){
        self.state = .fulfilled(value: value)
    }

    public init(error: Error){
        self.state = .rejected(error: error)
    }

    public convenience init(queue: DispatchQueue = DispatchQueue.global(qos: .userInitiated), work: @escaping ( _ fulfille: @escaping (Value) -> (), _ reject: @escaping (Error) -> ()) throws -> ()){
        self.init()
        queue.async(execute: {
            do{
                try work(self.fulfill, self.reject)
            }catch {
                self.reject(error)
            }
        })
    }

    /// - note: This one is "flatMap"
    @discardableResult
    public func then<NewValue>(on queue: ExecutionContext = DispatchQueue.main, _ onFulfilled: @escaping (Value) throws -> Promise<NewValue>) -> Promise<NewValue>{
        return Promise<NewValue>(work: {fulfill, reject in
            self.addCallbacks(on: queue,
                    onFulfilled: { value in
                        do{
                            try onFulfilled(value).then(on: queue, fulfill, reject)
                        } catch let error{
                            self.reject(error)
                        }
                    }, onRejected: reject)
        })
    }

    /// - note: This one is "map"
    @discardableResult
    public func then<NewValue>(on queue: ExecutionContext = DispatchQueue.main, _ onFulfilled: @escaping (Value) throws -> NewValue) -> Promise<NewValue> {
        return then(on: queue, { (value) -> Promise<NewValue> in
            do {
                return Promise<NewValue>(value: try onFulfilled(value))
            } catch let error {
                return Promise<NewValue>(error: error)
            }
        })
    }

    public func `catch`(on queue: ExecutionContext = DispatchQueue.main, _ onRejected: @escaping (Error) -> ()) -> Promise<Value> {
        return self.then(on: queue, {_ in }, onRejected)
    }

    @discardableResult
    public func then(on queue: ExecutionContext = DispatchQueue.main, _ onFulfilled: @escaping (Value) -> (), _ onRejected: @escaping (Error) -> () = { _ in }) -> Promise<Value> {
        self.addCallbacks(on: queue, onFulfilled: onFulfilled, onRejected: onRejected)
        return self
    }
    public func reject(_ error: Error){
        self.updateState(.rejected(error: error))
    }

    public func fulfill(_ value: Value){
        self.updateState(.fulfilled(value: value))
    }

    private func updateState(_ state: State<Value>){
        guard self.isPending else{ return }

        self.lockQueue.sync(execute: {
            self.state = state
        })
        self.fireCallbacksIfCompleted()
    }

    private func addCallbacks(on queue: ExecutionContext = DispatchQueue.main, onFulfilled: @escaping (Value) -> (), onRejected: @escaping (Error) -> ()) {
        let callback = CallBack(onFulfilled: onFulfilled, onRejected: onRejected, queue: queue)
        self.lockQueue.async(execute: {
            self.callbacks.append(callback)
        })

        self.fireCallbacksIfCompleted()
    }

    private func fireCallbacksIfCompleted(){
        self.lockQueue.async(execute: {
            guard !self.state.isPending else { return }
            self.callbacks.forEach { callback in
                switch self.state {
                    case let .fulfilled(value):
                        callback.callFulfill(value)
                    case let .rejected(error):
                        callback.callReject(error)
                    default:
                        break
                }
            }
            self.callbacks.removeAll()
        })
    }
}
